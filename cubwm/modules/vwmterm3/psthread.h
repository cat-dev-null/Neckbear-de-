#ifndef _PSTHREAD_H_
#define _PSHTREAD_H_

#include <stdint.h>

#include <glib.h>

#include <pseudo.h>

int16_t vwmterm_psthread(ps_task_t *task,void *anything);

#endif
