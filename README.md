=== Neckbear(de) ===
Neckbear(de) basically tries to bring the exact same experience of a Graphical Desktop Environment...
...only without X.

You might be wondering, "why do would anyone do this?" Good question.
  * Helps people to get comfortable with the command line, by providing an easy way to do certain tasks, making the 
learning curve much less intimidating.
  * Helps to expose people know to CLI apps that can perform just the same as desktop apps by giving a straightforward 
menu.
  * Speed: all of the glory of the Desktop paradimn without all that memory wasted to graphics.
  * Why the hell not?

